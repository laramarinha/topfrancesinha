FROM node:6.10.3

RUN mkdir /src

RUN npm install express-generator -g
RUN npm install yql -g

WORKDIR /src
ADD app/package.json /src/package.json
RUN npm install

EXPOSE 3000

CMD node app/bin/www
