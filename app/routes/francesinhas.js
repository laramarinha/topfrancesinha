var express = require('express');
var router = express.Router();
var YQL = require('yql');
var forEach = require('async-foreach').forEach;

/* GET users listing. */
router.get('/top5', function(req, res, next) {
  getTop5().then(function(result) {
      res.send(result);
    }),
    function(error) {
      res.send("ERROR!! Something went wrong")
    }
});


var getTop5 = function() {
  return new Promise(function(resolve, reject) {
    var restaurantList = [];

    var host = "http://francesinhas.com";
    var queryTop = 'select * from html where url="' + host + '/restaurante/search?name=&location=" and xpath="//div[@class=\'results\']/table/tbody/tr/td"';
    executeQuery(queryTop).then(function(response) {
      forEach(response.query.results.td, function(restaurant, index, array) {
        var newRestaurant = {};
        newRestaurant.name = restaurant.a.content;
        newRestaurant.address = restaurant.content.trim();
        var queryDetails = 'select * from html where url="' + host + restaurant.a.href + '" and xpath="//div[@class=\'details round-border-shadow\']/div"';
        executeQuery(queryDetails).then(function(response) {
          if (response.query.results != null || response.query.results != undefined) {
            forEach(response.query.results.div, function(restaurantDetail, index, array) {
              if (index == 1) {
                newRestaurant.contact = restaurantDetail.ul.li[1].span.content;
                if (restaurantDetail.ul.li[3] !== undefined) {
                  newRestaurant.website = restaurantDetail.ul.li[3].span.a.href;
                }
              }
            });
            restaurantList.push(newRestaurant);
            if (restaurantList.length == 5) {
              resolve(restaurantList);
            }
          }
        });
      });
    });
  })

}
var executeQuery = function(query_url) {
  return new Promise(function(resolve, reject) {
    var query = new YQL(query_url);
    query.exec(function(error, response) {
      if (error != null) {
        reject(error);
      } else {
        resolve(response);
      }
    });
  });
}
module.exports = router;
